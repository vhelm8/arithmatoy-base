#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }
char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs)
{
	char	*res;
	int	lenl = strlen(lhs);
	int	lenr = strlen(rhs);
	const char	*great = (lenl >= lenr ? lhs : rhs);
	const char	*small = (lenl >= lenr ? rhs : lhs);
	int	len;

	if (VERBOSE) { fprintf(stderr, "add: entering function\n"); }

	lenl = strlen(great);
	lenr = strlen(small);
	len = lenl + 1;

	if (!(res = (char *)malloc(sizeof(*res) * (len + 1))))
		return (NULL);
	memset(res, '0', len);
	res[len] = 0;

	len--;
	lenl--;
	lenr--;

	int tmp = 0;
	while (len > 0)
	{
		if (VERBOSE) { fprintf(stderr, "add: digit %c digit %c carry %c\n", (lenl >= 0 ? great[lenl] : '0'), (lenr >= 0 ? small[lenr] : '0'), res[len]); }

		tmp = get_digit_value(res[len])
			+ (lenl >= 0 ? get_digit_value(great[lenl]) : 0)
			+ (lenr >= 0 ? get_digit_value(small[lenr]) : 0);

		res[len - 1] = to_digit(tmp / base);
		res[len] = to_digit(tmp % base);	

		if (VERBOSE) { fprintf(stderr, "add: result: digit %c carry %c\n", res[len], res[len - 1]); }

		len--;
		lenl--;
		lenr--;
	}
	if (get_digit_value(res[0]) != 0)
		if (VERBOSE) { fprintf(stderr, "add: final carry %c\n", res[0]); }

	while (*res == '0' && res[1] != 0)
		res = res + 1;

	return (res);
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs)
{
	char    *res;
	int     lenl = strlen(drop_leading_zeros(lhs));
	int     lenr = strlen(drop_leading_zeros(rhs));
	const char      *great = (lenl >= lenr ? drop_leading_zeros(lhs) : drop_leading_zeros(rhs));
	const char      *small = (lenl >= lenr ? drop_leading_zeros(rhs) : drop_leading_zeros(lhs));
	int     len;

	if (VERBOSE) { fprintf(stderr, "sub: entering function\n"); }

	lenl = strlen(great);
	lenr = strlen(small);
	len = lenl + 1;

	if (!(res = (char *)malloc(sizeof(*res) * (len + 1))))
		return (NULL);
	memset(res, '0', len);
	res[len] = 0;

	len--;
	lenl--;
	lenr--;

	int tmp = 0;
	while (len > 0)
	{
		if (VERBOSE) { fprintf(stderr, "sub: digit %c digit %c carry %c\n", (lenl >= 0 ? great[lenl] : '0'), (lenr >= 0 ? small[lenr] : '0'), res[len]); }

		tmp = ((lenl >= 0 ? get_digit_value(great[lenl]) : 0)
				- get_digit_value(res[len])
				- (lenr >= 0 ? get_digit_value(small[lenr]) : 0));
		res[len - 1] = to_digit(tmp < 0 ? 1 : 0);
		res[len] = to_digit(tmp < 0 ? (tmp + base) % base : tmp % base);

		if (VERBOSE) { fprintf(stderr, "sub: result: digit %c carry %c\n", res[len], res[len - 1]); }

		len--;
		lenl--;
		lenr--;
	}

	if (get_digit_value(res[len]) != 0)
		if (VERBOSE) { fprintf(stderr, "sub: final carry %c\n", res[len]); }

	if (res[len] == '1')
		return (NULL);

	while (*res == '0' && res[1] != 0)
		res = res + 1;

	return (res);
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs)
{
	char    *res;
	char	*carry;
	int     len;
	int     lenl = strlen(lhs);
	int     lenr = strlen(rhs);
	const char	*great = (lenl >= lenr ? lhs : rhs);
	const char	*small = (lenl >= lenr ? rhs : lhs);	

	if (VERBOSE) {
		fprintf(stderr, "mul: entering function\n");
	}

	lenl = strlen(great);
	lenr = strlen(small);
	len = lenl + lenr;

	if (!(res = (char *)malloc(sizeof(*res) * (len + 1))))
		return (NULL);
	memset(res, '0', len);
	res[len + 1] = 0;
	lenl++;
	if (!(carry = (char *)malloc(sizeof(*carry) * (lenl + 1))))
		return (NULL);
	memset(carry, '0', lenl);
	carry[lenl] = 0;

	len--;
	lenl--;
	lenr--;

	int	tmp = 0;
	int	i = 0;
	while (lenr >= 0)
	{
		if (VERBOSE) { fprintf(stderr, "mul: digit %c number %s\n", small[lenr], great); }

		i = 0;
		while (i < lenl)
		{
			if (VERBOSE) { fprintf(stderr, "mul: digit %c digit %c carry %c\n", small[lenr], great[lenl - i - 1], carry[lenl - i]); }

			tmp = (get_digit_value(great[lenl - i - 1])
				* get_digit_value(small[lenr]))
				+ get_digit_value(carry[lenl - i]);
			carry[lenl - i - 1] = to_digit(tmp / base);
			carry[lenl - i] = to_digit(tmp % base);


			if (VERBOSE) { fprintf(stderr, "mul: result: digit %c carry %c\n", carry[lenl - i], carry[lenl - i - 1]); }

			i++;
		}
		
		if (get_digit_value(carry[lenl - i]) != 0)
			if (VERBOSE) { fprintf(stderr, "mul: final carry %c\n", carry[lenl - i]); }
		
		if (VERBOSE) { fprintf(stderr, "mul: add %s + %s\n", drop_leading_zeros(res), (carry[0] == '0' ? carry + 1 : carry)); }
		
		if (strlen(drop_leading_zeros(carry)) != strlen(drop_leading_zeros(res)))
			res = arithmatoy_add(base, drop_leading_zeros(carry), drop_leading_zeros(res));
		else
			res = arithmatoy_add(base, drop_leading_zeros(res), drop_leading_zeros(carry));
		
		if (VERBOSE) { fprintf(stderr, "mul: result: %s\n", drop_leading_zeros(res)); }
		
		memset(carry, '0', strlen(carry) + 1);
		carry[strlen(carry) + 1] = 0;

		len--;
		lenr--;
	}

	while (*res == '0' && res[1] != 0)
		res = res + 1;
	return (res);
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
	// Convert a digit from get_all_digits() to its integer value
	if (digit >= '0' && digit <= '9') {
		return digit - '0';
	}
	if (digit >= 'a' && digit <= 'z') {
		return 10 + (digit - 'a');
	}
	return -1;
}

char to_digit(unsigned int value) {
	// Convert an integer value to a digit from get_all_digits()
	if (value >= ALL_DIGIT_COUNT) {
		debug_abort("Invalid value for to_digit()");
		return 0;
	}
	return get_all_digits()[value];
}

char *reverse(char *str) {
	// Reverse a string in place, return the pointer for convenience
	// Might be helpful if you fill your char* buffer from left to right
	const size_t length = strlen(str);
	const size_t bound = length / 2;
	for (size_t i = 0; i < bound; ++i) {
		char tmp = str[i];
		const size_t mirror = length - i - 1;
		str[i] = str[mirror];
		str[mirror] = tmp;
	}
	return str;
}

const char *drop_leading_zeros(const char *number) {
	// If the number has leading zeros, return a pointer past these zeros
	// Might be helpful to avoid computing a result with leading zeros
	if (*number == '\0') {
		return number;
	}
	while (*number == '0') {
		++number;
	}
	if (*number == '\0') {
		--number;
	}
	return number;
}

void debug_abort(const char *debug_msg) {
	// Print a message and exit
	fprintf(stderr, debug_msg);
	exit(EXIT_FAILURE);
}
